# EO CEO
PPW Odd 2020/2021 - Story 6

## Overview
EO CEO is an application that can be used to manage events and participants. User inputs a new event in main page form and adds participants in event form. Using Django model relationship, each participant is bound to one event. It implements Django's test driven development (TDD) to make sure that no bugs can occur. Minimum test coverage is 90%.

## Link
Click <http://jojostory6.herokuapp.com> to get to the website.

## Resources
- HTML
- CSS
- Bootstrap
- Django
- Heroku