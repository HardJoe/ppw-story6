from django.test import TestCase, Client
from django.urls import reverse, resolve

from .models import Event, Member


class EventTestCase(TestCase):
    def setUp(self):
        self.client = Client()
        self.event_url = reverse("story6:index")
        self.test_event = Event.objects.create(
            title="Minum boba"
        )
        self.test_member = Member.objects.create(
            name="Izuri",
            event=self.test_event
        )
        self.test_delete_url = reverse(
            "story6:delete_event",
            args=[self.test_event.id]
        )
        self.test_member_delete_url = reverse(
            "story6:delete_member",
            args=[self.test_member.id]
        )

    def test_event_GET(self):
        response = self.client.get(self.event_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "story6/index.html")

    def test_event_POST(self):
        response = self.client.post(self.event_url, {
            "title": "Mabar Genshin"
        }, follow=True)
        self.assertContains(response, "Mabar Genshin")

    def test_member_POST(self):
        response = self.client.post(self.event_url, {
            "name": "Dio",
            "event": self.test_event.id,
        }, follow=True)
        self.assertContains(response, "Dio")

    def test_notValid_POST(self):
        response = self.client.post(self.event_url, {
            "name": "Chad",
            "event": "Simping",
        }, follow=True)
        self.assertContains(response, "Invalid input")

    def test_member_DELETE(self):
        response = self.client.get(self.test_member_delete_url, follow=True)
        print(response.content)
        self.assertContains(response, "deleted")

    def test_event_DELETE(self):
        response = self.client.get(self.test_delete_url, follow=True)
        self.assertContains(response, "deleted")
