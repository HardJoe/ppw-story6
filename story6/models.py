from django.db import models


class Event(models.Model):
    title = models.CharField(max_length=100)

    def __str__(self):
        return self.title


class Member(models.Model):
    name = models.CharField(max_length=100)
    event = models.ForeignKey(Event, on_delete=models.CASCADE)

    def __str__(self):
        return self.name
