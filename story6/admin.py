from django.contrib import admin

from .models import Event, Member


class MemberInline(admin.StackedInline):
    model = Member


@admin.register(Event)
class EventAdmin(admin.ModelAdmin):
    inlines = [MemberInline, ]
