from django.urls import path

from . import views

app_name = 'story6'

urlpatterns = [
    path('', views.index, name='index'),
    path('delete/<str:pk>', views.delete_event, name='delete_event'),
    path('delete/member/<str:pk>', views.delete_member, name='delete_member'),
]
